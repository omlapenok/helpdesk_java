package ru.edu.helpdesk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Категории обращений
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Title {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean mark = false;

    @Override
    public String toString() {
        return name;
    }
}
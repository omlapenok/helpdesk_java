package ru.edu.helpdesk.entity;

/**
 * Статусы, в которых может находится обращение
 */
public enum TicketStatus {
    OPEN,
    WORKING,
    COMPLETED,
    REJECTED

}

package ru.edu.helpdesk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import ru.edu.helpdesk.security.MySimpleUrlAuthenticationSuccessHandler;

/**
 * Подключает Spring Security в приложении
 */
@Configuration
@EnableWebSecurity
public class HelpdeskSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Метод HelpdeskSecurityConfig#configure(HttpSecurity http) конфигурирует доступ к страницам приложения в зависимости от роли пользователя
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().ignoringAntMatchers()
                .and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/registration").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(myAuthenticationSuccessHandler());

    }

    /**
     * HelpdeskSecurityConfig#configure(WebSecurity web) дает доступ к вставке картинок в приложение
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(

                // статика
                "/img/**"
        );
    }

    /**
     * Определяет кастомный обработчик успешного перехода на ту или иную страницу в зависимости от роли пользователя
     *
     * @return
     */
    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new MySimpleUrlAuthenticationSuccessHandler();
    }


    /**
     * Хеширует пароли всех пользователей
     *
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}

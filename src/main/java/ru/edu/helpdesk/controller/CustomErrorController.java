package ru.edu.helpdesk.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Custom ErrorController
 */
@Controller
public class CustomErrorController implements ErrorController {
    /**
     * Error page
     */
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.FORBIDDEN.value()) {

                return "error/access_denied";
            } else if (statusCode == HttpStatus.NOT_FOUND.value()) {

                return "error/not_found";
            }
        }

        return "error/error";
    }
}

package ru.edu.helpdesk.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.form.UserEditForm;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.AdminService;

/**
 * Контроллер для администратора (доступен только для роли администратор)
 */
@Slf4j
@Controller
@Secured("ROLE_ADMIN")
@RequestMapping(value = "/admin")
public class AdminController {

    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /**
     * Список пользователей и категорий - страница
     */
    @GetMapping()
    public String index(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        log.info("GET '/admin'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        return "admin/index";
    }

    /**
     * Список пользователей - страница
     */
    @GetMapping("/user")
    public String userList(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        log.info("GET '/admin/user'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        model.addAttribute("users", adminService.allUsers());
        return "admin/user_list";
    }

    /**
     * Редактирование пользователя - страница
     */
    @GetMapping("/user/{id}")
    public String userEdit(Model model,
                           @AuthenticationPrincipal HelpdeskUserPrincipal principal,
                           @PathVariable("id") Long id) {
        log.info("GET '/admin/user/" + id + "'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        model.addAttribute("user", adminService.getUserById(id));
        return "admin/user_edit";
    }

    /**
     * Редактирование пользователя - сохранение изменений
     */
    @PostMapping("/user/{id}")
    public String userEditPost(@ModelAttribute("user") UserEditForm userEditForm) {
        log.info("POST '/admin/user/" + userEditForm.getId() + "'");
        adminService.updateUser(userEditForm);
        return "redirect:/admin/user";
    }

    /**
     * Список категорий - страница
     */
    @GetMapping("/title")
    public String titleList(Model model,
                            @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        log.info("GET '/admin/title'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        model.addAttribute("titles", adminService.allTitles());
        return "admin/title_list";
    }

    /**
     * Редактирование категории - страница
     */
    @GetMapping("/title/{id}")
    public String titleEdit(Model model,
                            @AuthenticationPrincipal HelpdeskUserPrincipal principal,
                            @PathVariable("id") Long id) {
        log.info("GET '/admin/title/" + id + "'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        model.addAttribute("title", adminService.getTitleById(id));
        return "admin/title_edit";
    }

    /**
     * Редактирование категории - сохранение изменений
     */
    @PostMapping("/title/{id}")
    public String titleEditPost(@ModelAttribute("title") Title title) {
        log.info("POST '/admin/title/" + title.getId() + "'");
        adminService.saveTitle(title);
        return "redirect:/admin/title";
    }

    /**
     * Удаление категории
     */
    @PostMapping("/title/delete/{id}")
    public String titleDelete(Model model,
                              @AuthenticationPrincipal HelpdeskUserPrincipal principal,
                              @PathVariable("id") Long id) {
        log.info("POST '/admin/title/delete/" + id + "'");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        try {
            adminService.deleteTitleById(id);
        } catch (Exception e) {
            log.error("message = " + e.getMessage());
            model.addAttribute("message", e.getMessage());
        }
        model.addAttribute("titles", adminService.allTitles());
        return "admin/title_list";
    }

    /**
     * Создание новой категории - страница
     */
    @GetMapping("/title/new")
    public String titleNew(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        log.info("GET '/admin/title/new");
        if (principal != null) {
            model.addAttribute("current", principal.getUser());
        }
        model.addAttribute("title", new Title());
        return "admin/title_new";
    }

    /**
     * Создание новой категории - сохранение изменений
     */
    @PostMapping("/title/new")
    public String titleNewPost(@ModelAttribute("title") Title title) {
        log.info("POST '/admin/title/new");
        adminService.saveTitle(title);
        return "redirect:/admin/title";
    }
}

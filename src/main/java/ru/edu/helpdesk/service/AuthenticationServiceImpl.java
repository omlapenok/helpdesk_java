package ru.edu.helpdesk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.repository.UserRepository;

/**
 * Сервис, реализующий связь между таблицей user базы данных и интерфейса
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;

    @Autowired
    public AuthenticationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Метод AuthenticationServiceImpl#saveUser(User user) сохраняет нового пользователя в БД
     *
     * @param user новый пользователь для вставки в БД
     */
    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    /**
     * Метод AuthenticationServiceImpl#getUserByUsername(String username) ищет пользователя по логину
     *
     * @param username логин искомого пользователя
     * @return возвращает нужного нам пользователя или NULL если пользователь не найден
     */
    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByLogin(username);
    }
}

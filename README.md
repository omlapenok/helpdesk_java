![](https://ru.inettools.net/upload/SO1f2ZIbRotdsA3O92Daq2lm7nAAopl7j1EwwnmB/98339434-helpdesk-logo-template-support-service-vector-design-call-center-logotype.5hIJn.webp)
## Client-server application
### Developers:
- 🐱‍🏍**Лапенок Ольга** (https://bitbucket.org/omlapenok/)
- 🐱‍💻**Малышев Сергей**
- 🐱‍👤**Микишев Артем**
- 🐱‍👓**Садиков Александр** (https://bitbucket.org/alsadi1982/)

## About project

Helpdesk – это система, предназначенная для автоматизации обработки запросов клиентов на техническое обслуживание.
Благодаря внедрению системы Helpdesk, пользователи получают возможность оперативно создавать обращения в службу техподдержки, а сотрудники техподдержки решать неисправности, запрашивать у пользователей дополнительную информацию о проблеме. Руководство компании в свою очередь может получить информацию о кол-ве неисправностей и эффективности их устранения.

## Used technologies 
- **Spring Boot**
- **Spring Security**
- **Spring AOP**
- **Spring Data**
- **Apache Tomcat**
- **Rest API**
- **HTML**
- **CSS**
- **Thymleaf**
- **PostgreSQL**
- **Log4j**
- **Sonarqube**
- **Docker**

## Technical description
Для начала работы необходимо: - установить PostgeSQL - в файле resources\application.property указать данные для подключения к БД. Запустить HelpdeskApplication

Учетная запись администратора: login: admin password: admin Сотрудника поддержки: login: support password: support Пользователя: login: user1 password: user1